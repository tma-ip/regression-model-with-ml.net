﻿using System;
using System.IO;
using Microsoft.Data.DataView;
using Microsoft.ML;
using Microsoft.ML.Data;

namespace SHCarPrediction
{
    class Program
    {
        static readonly string _trainDataPath = Path.Combine("../../../Data", "auto_cleanedTrain.csv");
        static readonly string _testDataPath = Path.Combine("../../../Data", "auto_cleanedTest.csv");
        static readonly string _modelPath = Path.Combine("../../../Data", "Model.zip");

        static void Main(string[] args)
        {
            MLContext mlContext = new MLContext(seed: 0);
            var model = Train(mlContext, _trainDataPath);
            Evaluate(mlContext, model);
            TestSinglePrediction(mlContext);
        }

        public static ITransformer Train(MLContext mlContext, string dataPath)
        {
            IDataView dataView = mlContext.Data.LoadFromTextFile<Car>(dataPath, hasHeader: true, separatorChar: ',');
            var pipeline = mlContext.Transforms.CopyColumns(outputColumnName: "Label", inputColumnName: "Price")
                                    .Append(mlContext.Transforms.Categorical.OneHotEncoding(outputColumnName: "GearboxEncoded", inputColumnName: "Gearbox"))
                                    .Append(mlContext.Transforms.Categorical.OneHotEncoding(outputColumnName: "ModelEncoded", inputColumnName: "Model"))
                                    .Append(mlContext.Transforms.Categorical.OneHotEncoding(outputColumnName: "FuelTypeEncoded", inputColumnName: "FuelType"))
                                    .Append(mlContext.Transforms.Categorical.OneHotEncoding(outputColumnName: "BrandEncoded", inputColumnName: "Brand"))
                                    .Append(mlContext.Transforms.Concatenate("Features", "YearOfRegistration", "GearboxEncoded", "ModelEncoded", "Kilometer", "FuelTypeEncoded", "BrandEncoded"))
                                    .Append(mlContext.Regression.Trainers.PoissonRegression());
            //Inainte de curatare
            //FastTree => r2 = -0.3 si rms = 614300.17 => prost
            //FastForest => r2 = 0 si rms = 60700 => prost
            //PoissonRegression => r2 = 0 si rms = 60664
            //dupa curatare
            //Poisson R2 = 0.21, rms = 7435.65

            var model = pipeline.Fit(dataView);
            SaveModelAsFile(mlContext, model);
            return model;
        }

        private static void SaveModelAsFile(MLContext mlContext, ITransformer model)
        {
            using (var fileStream = new FileStream(_modelPath, FileMode.Create, FileAccess.Write, FileShare.Write))
                mlContext.Model.Save(model, fileStream);
            Console.WriteLine("The model is saved to {0}", _modelPath);
        }

        private static void Evaluate(MLContext mlContext, ITransformer model)
        {
            IDataView dataView = mlContext.Data.LoadFromTextFile<Car>(_testDataPath, hasHeader: true, separatorChar: ',');
            var predictions = model.Transform(dataView);
            var metrics = mlContext.Regression.Evaluate(predictions, "Label", "Score");

            Console.WriteLine();
            Console.WriteLine($"*************************************************");
            Console.WriteLine($"*       Model quality metrics evaluation         ");
            Console.WriteLine($"*------------------------------------------------");

            Console.WriteLine($"*       R2 Score:      {metrics.RSquared:0.##}");
            Console.WriteLine($"*       RMS loss:      {metrics.Rms:#.##}");
            Console.ReadKey();

        }

        private static void TestSinglePrediction(MLContext mlContext)
        {
            ITransformer loadedModel;
            using (var stream = new FileStream(_modelPath, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                loadedModel = mlContext.Model.Load(stream);
            }

            var predictionFunction = loadedModel.CreatePredictionEngine<Car, CarPricePrediction>(mlContext);
            var carSample = new Car()
            {
                Price = 0,
                YearOfRegistration = 2011,
                Gearbox = "manuell",
                Model = "tiguan",
                Kilometer = 30000,
                FuelType = "benzin",
                Brand = "volkswagen"

            };
            var prediction = predictionFunction.Predict(carSample);
            Console.WriteLine($"**********************************************************************");
            Console.WriteLine($"Predicted fare: {prediction.Price:0.####}, actual fare: 16150");
            Console.WriteLine($"**********************************************************************");
            Console.ReadKey();
        }
    }
}
