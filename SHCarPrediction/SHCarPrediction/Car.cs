﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Microsoft.ML.Data;

namespace SHCarPrediction
{
    public class Car
    {
        [LoadColumn(1)]
        public float Price;

        [LoadColumn(2)]
        public float YearOfRegistration;

        [LoadColumn(3)]
        public string Gearbox;

        [LoadColumn(4)]
        public string Model;

        [LoadColumn(5)]
        public float Kilometer;

        [LoadColumn(6)]
        public string FuelType;

        [LoadColumn(7)]
        public string Brand;
    }

    public class CarPricePrediction
    {
        [ColumnName("Score")]
        public float Price;
    }
}
