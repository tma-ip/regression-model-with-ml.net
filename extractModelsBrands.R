fpath = "C:/Users/anama/Desktop/auto_cleaned.csv"
library(gdata)
library(data.table)
library(gplots)
require(gplots)

data <- data.table::fread(fpath, na.strings = "",quote="", stringsAsFactors = TRUE)

models <- unique(data[,"model"])
brands <- unique(data[, "brand"])
models <- models[order(model)]
brands <- brands[order(brand)]
writeText <- vector()
for (i in 2:dim(brands)[1])
{
  writeText = c(writeText, "------", as.character(brands[i,brand]))
  models1 <- vector()
  models1 <- subset(data, as.character(brand) == as.character(brands[i,brand]))
  models1 <- unique(models1[, "model"])
  models1 <- models1[order(model)]
  for( j in 1:dim(models1)[1])
  {
    writeText <- c(writeText, as.character(models1[j,model]))
  }
}

fileConn<-file("ModelsAndBrands.txt")
writeLines(writeText, fileConn)
close(fileConn)